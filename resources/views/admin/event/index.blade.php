@extends('layouts.app')

@section('content')

<div class="container">
	
	<a href="{{url('admin/event/create')}}">Add new Event</a>

	<table class="table table-bordered">
		<tr>
			<td>No</td>
			<td>Nama</td>
			<td>Tanggal</td>
			<td>Action</td>
		</tr>

		@foreach($events as $event)
		<tr>
			<td>{{$loop->iteration}}</td>
			<td>{{$event->nama}}</td>
			<td>{{$event->tanggal}}</td>
			<td>
				<a href="{{url('admin/event/' . $event->id . '/edit')}}">Update</a>
				<form action="{{url('admin/event/' . $event->id )}}" method="POST">
					{{ csrf_field() }}
					{{ method_field('DELETE') }}
					<button type="submit">Delete</button>
				</form>
			</td>
		</tr>
		@endforeach

	</table>

</div>

@endsection