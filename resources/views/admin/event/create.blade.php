@extends('layouts.app')

@section('content')

<div class="container">
	<form action="{{url('admin/event/')}}" method="POST">
		{{csrf_field()}}
		<label>Nama Event</label>
		<input type="text" name="nama">
		
		<br>

		<label>Tanggal Event</label>
		<input type="date" name="date">

		<input type="submit" name="submit" class="btn btn-success">
	</form>
</div>

@endsection