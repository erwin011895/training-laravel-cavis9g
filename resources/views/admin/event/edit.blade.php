@extends('layouts.app')

@section('content')

<div class="container">
	<form action="{{url('admin/event/' . $event->id )}}" method="POST">
		{{csrf_field()}}

		{{method_field('PUT')}}

		<label>Nama Event</label>
		<input type="text" name="nama" value="{{$event->nama}}">
		
		<br>

		<label>Tanggal Event</label>
		<input type="date" name="date" value="{{$event->tanggal}}">

		<input type="submit" name="submit" class="btn btn-success">
	</form>
</div>

@endsection