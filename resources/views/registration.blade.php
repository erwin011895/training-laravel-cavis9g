<!DOCTYPE html>
<html>
<head>
	<title>Register Event Birthday Party</title>
	<link rel="stylesheet" type="text/css" href="{{asset('css/bootstrap.min.css')}}">

</head>
<body>
	<div class="container">
		<h1>Registrasi Event Birthday Party</h1>
		<div class="col-md-6">
			<form>
			  <div class="form-group">
			    <label>NIM</label>
			    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Nim" name="nim">
			  </div>
			  <div class="form-group">
			    <label>Nama</label>
			    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
			  </div>
				
				<div class="form-group">
			    <label>Jurusan</label>
			    <select class="form-control" name="jurusan">
					  	<option>-</option>
				  	@foreach(['IT', 'MAT', 'SI', 'MMSI', 'MTI', 'SI-MN', 'SI-AK', 'Management', 'Akuntansi', 'International Marketing'] as $jurusan)
					  	<option value="{{$jurusan}}">{{$jurusan}}</option>
						@endforeach
					</select>
					  
			  </div>

			  <div class="form-group">
			  	<label>Bulan</label>
			  	<select name="bulan">
			  		@foreach( [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12] as $b )
				  		<option>{{$b}}</option>
				  	@endforeach
			  	</select>
			  </div>

				<div class="form-group">
					<div>
			    	<label>Konfirmasi</label>
					</div>
			    <label class="radio-inline">
					  <input type="radio" name="konfirmasi" id="inlineRadio1" value="Hadir"> Hadir
					</label>
					<label class="radio-inline">
					  <input type="radio" name="konfirmasi" id="inlineRadio2" value="Tidak Hadir"> Tidak Hadir
					</label>
			  </div>
				


			  <button type="submit" class="btn btn-primary">Submit</button>
			</form>
		</div>
	</div>
</body>
</html>