<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User; //include atau import class app/User.php
use Illuminate\Support\Facades\DB; //import class DB Laravel

class PlaygroundController extends Controller
{
    public function index()
    {
    	$users = DB::table('users')->get();
        dd($users);
    	// $users = DB::table('users')->where('name', 'Mr. Sylvester Abernathy')->first();
    	// $users = User::find(12);
    	
    	// $user = new User; 
    	// $user->name = "Steven I.J.";
    	// $user->email = "steven@gmail.com";
    	// $user->password = bcrypt( "SECRET" );
    	// $user->save();
    	 
    	// $user = User::find(102);
    	// $user->name = "Steven Ivander J.";
    	// $user->save();
    	
    	// dd('user inserted');

    	return view('playground.index');
    }
}
