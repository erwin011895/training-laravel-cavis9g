<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/register', 'RegistrationController@index');

Route::group(['prefix' => 'admin'], function(){
	Auth::routes();

	Route::get('/home', 'HomeController@index');

	Route::resource('event', 'EventController');
	// Route::get('/event', 'EventController@index');
	// Route::get('/event/create', 'EventController@create');
	// Route::post('/event', 'EventController@store');
	// Route::get('/event/{event}/edit', 'EventController@edit');
	// Route::put('/event/{event}', 'EventController@update');
	// Route::delete('/event/{event}', 'EventController@destroy');
});

Route::get('db-playground', 'PlaygroundController@index');